﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiMicromania.Data
{
    public class MicromaniaContext : DbContext
    {
        public MicromaniaContext(DbContextOptions<MicromaniaContext> opt) : base(opt)
        {
            
        }

        public DbSet<Game> Games { get; set; }

        public DbSet<Users> Users { get; set; }

        public DbSet<Collections> Collections { get; set; }

        public DbSet<GameCollection> GameCollection { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GameCollection>()
                .HasKey(gc => new {gc.GameId, gc.CollectionId});
            modelBuilder.Entity<GameCollection>()
                .HasOne(gc => gc.Game)
                .WithMany(g => g.Collection)
                .HasForeignKey(gc => gc.GameId);
            modelBuilder.Entity<GameCollection>()
                .HasOne(gc => gc.Collection)
                .WithMany(c => c.Games)
                .HasForeignKey(gc => gc.CollectionId);
        }
    }
}
