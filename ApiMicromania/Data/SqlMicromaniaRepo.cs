﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Models;
using Microsoft.EntityFrameworkCore;

namespace ApiMicromania.Data
{
    public class SqlMicromaniaRepo : IGamesRepo
    {
        private readonly MicromaniaContext _context;

        public SqlMicromaniaRepo(MicromaniaContext context)
        {
            _context = context;
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() >= 0);
        }

        public IEnumerable<Game> GetAllGames()
        {
            return (_context.Games.ToList());
        }

        public Game GetGameById(int id)
        {
            return _context.Games.FirstOrDefault(p => p.Id == id);
        }

        public void CreateGame(Game game)
        {
            if (game == null)
            {
                throw new ArgumentNullException(nameof(game));
            }

            _context.Games.Add(game);
        }

        public void UpdateGame(Game game)
        { 
        }

        public void DeleteGame(Game game)
        {
            if (game == null)
            {
                throw new ArgumentNullException(nameof(game));
            }

            _context.Games.Remove(game);
        }

        public IEnumerable<Collections> GetAllCollections()
        {
            
            return (_context.Collections.ToList());
        }

        public void CreateCollection(Collections collection)
        {
            if (collection == null)
            {
                throw new NotImplementedException();
            }
            _context.Collections.Add(collection);
        }

        public void UpdateCollection(Collections collection)
        {
        }

        public void DeleteCollection(Collections collection)
        {
            if (collection == null)
            {
                throw new NotImplementedException();
            }

            _context.Collections.Remove(collection);
        }

        public IEnumerable<GameCollection> GetCollectionGame(int idCollection)
        {
            List<GameCollection> games = _context.GameCollection.Where(c => c.CollectionId == idCollection)
                .Include(gc => gc.Game).ToList();
            return games;
        }

        public async Task<IEnumerable<GameCollection>> GetCollectionGamesAsync(int collectionId)
        {
            return await _context.GameCollection.Include(g => g.Game).Where(c => c.CollectionId == collectionId)
                .ToListAsync();
        }

        public GameCollection GetCollectionGame(int idCollection, int idGame)
        {
            GameCollection gameCollection =
                _context.GameCollection.FirstOrDefault(g => (g.GameId == idGame && g.CollectionId == idCollection));
            if (gameCollection == null)
            {
                throw new ArgumentNullException(nameof(gameCollection));
            }

            return gameCollection;
        }

        public void UpdateGameCollection(GameCollection gameCollection)
        {
        }

        public IEnumerable<GameCollection> getGameCollection(int idGame)
        {
            List<GameCollection> collection = _context.GameCollection.Include(g => g.Collection)
                .Where(c => c.GameId == idGame).ToList();
            return collection;
        }

        public void AddGameToCollection(GameCollection gameCollectionAdd)
        {
            if (gameCollectionAdd == null)
            {
                throw new ArgumentNullException(nameof(gameCollectionAdd));
            }

            _context.GameCollection.Add(gameCollectionAdd);
        }

        public void RemoveGameFromCollection(GameCollection gameCollection)
        {
            if (gameCollection == null)
            {
                throw new ArgumentNullException(nameof(gameCollection));
            }

            _context.GameCollection.Remove(gameCollection);
        }

        public Collections GetCollectionById(int id)
        {
            return _context.Collections.FirstOrDefault(i => i.Id == id);
        }
    }
}
