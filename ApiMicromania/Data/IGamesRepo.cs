﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Models;

namespace ApiMicromania.Data
{
    public interface IGamesRepo
    {
        bool SaveChanges();
        
        //Games
        IEnumerable<Game> GetAllGames();
        Game GetGameById(int id);
        void CreateGame(Game game);
        void UpdateGame(Game game);
        void DeleteGame(Game game);
        
        //Collections
        IEnumerable<Collections> GetAllCollections();
        Collections GetCollectionById(int id);
        void CreateCollection(Collections collection);
        void UpdateCollection(Collections collection);
        void DeleteCollection(Collections collection);

        //GameCollection
        IEnumerable<GameCollection> GetCollectionGame(int idCollection);
        Task<IEnumerable<GameCollection>> GetCollectionGamesAsync(int collectionId);
        GameCollection GetCollectionGame(int idCollection, int idGame);
        void UpdateGameCollection(GameCollection gameCollection);
        IEnumerable<GameCollection> getGameCollection(int idGame);
        void AddGameToCollection(GameCollection gameCollectionAdd);
        void RemoveGameFromCollection(GameCollection gameCollection);
        
    }
}
