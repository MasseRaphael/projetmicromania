﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Dtos;
using ApiMicromania.Models;
using AutoMapper;

namespace ApiMicromania.Profiles
{
    public class GamesProfile : Profile
    {
        public GamesProfile()
        {
            CreateMap<Game, GameReadDto>();
            CreateMap<GameCreateDto, Game>();
            CreateMap<GameUpdateDto, Game>();
            CreateMap<Game, GameUpdateDto>();
        }
    }
}
