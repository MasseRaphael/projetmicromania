﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Dtos;
using ApiMicromania.Models;
using AutoMapper;

namespace ApiMicromania.Profiles
{
    public class GameCollectionProfile : Profile
    {
        public GameCollectionProfile()
        {
            CreateMap<GameCollectionCreateDto, GameCollection>();
            CreateMap<Game, GameCollectionReadDto>();
            CreateMap<GameCollection, GameCollectionReadDto>();
        }
    }
}
