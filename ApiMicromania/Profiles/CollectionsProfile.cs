﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Dtos;
using ApiMicromania.Models;
using AutoMapper;

namespace ApiMicromania.Profiles
{
    public class CollectionsProfile : Profile
    {
        public CollectionsProfile()
        {
            CreateMap<Collections, CollectionsReadDto>();
            CreateMap<CollectionsCreateDto, Collections>();
            CreateMap<CollectionsUpdateDto, Collections>();
            CreateMap<Collections, CollectionsUpdateDto>();
        }
    }
}
