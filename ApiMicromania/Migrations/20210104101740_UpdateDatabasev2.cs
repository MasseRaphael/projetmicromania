﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiMicromania.Migrations
{
    public partial class UpdateDatabasev2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UsersId",
                table: "Collections",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Collections_UsersId",
                table: "Collections",
                column: "UsersId");

            migrationBuilder.AddForeignKey(
                name: "FK_Collections_Users_UsersId",
                table: "Collections",
                column: "UsersId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Collections_Users_UsersId",
                table: "Collections");

            migrationBuilder.DropIndex(
                name: "IX_Collections_UsersId",
                table: "Collections");

            migrationBuilder.DropColumn(
                name: "UsersId",
                table: "Collections");
        }
    }
}
