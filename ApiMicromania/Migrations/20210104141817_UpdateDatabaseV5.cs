﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiMicromania.Migrations
{
    public partial class UpdateDatabaseV5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Games_Collections_CollectionsId",
                table: "Games");

            migrationBuilder.DropIndex(
                name: "IX_Games_CollectionsId",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "CollectionsId",
                table: "Games");

            migrationBuilder.CreateTable(
                name: "GameCollection",
                columns: table => new
                {
                    GameId = table.Column<int>(nullable: false),
                    CollectionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCollection", x => new { x.GameId, x.CollectionId });
                    table.ForeignKey(
                        name: "FK_GameCollection_Collections_CollectionId",
                        column: x => x.CollectionId,
                        principalTable: "Collections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameCollection_Games_GameId",
                        column: x => x.GameId,
                        principalTable: "Games",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameCollection_CollectionId",
                table: "GameCollection",
                column: "CollectionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GameCollection");

            migrationBuilder.AddColumn<int>(
                name: "CollectionsId",
                table: "Games",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Games_CollectionsId",
                table: "Games",
                column: "CollectionsId");

            migrationBuilder.AddForeignKey(
                name: "FK_Games_Collections_CollectionsId",
                table: "Games",
                column: "CollectionsId",
                principalTable: "Collections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
