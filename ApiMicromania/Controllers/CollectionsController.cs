﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Data;
using ApiMicromania.Dtos;
using ApiMicromania.Models;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace ApiMicromania.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : Controller
    {
        private readonly IGamesRepo _repository;
        private readonly IMapper _mapper;

        public CollectionsController(IGamesRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        //GET api/Collections/{collectionID}/games
        [HttpGet("{collectionID}/games")]
        public async Task<ActionResult<IEnumerable<GameCollectionReadDto>>> GetCollectionGames(int collectionID)
        {
            IEnumerable<GameCollection> gameCollections = await _repository.GetCollectionGamesAsync(collectionID);
            return Ok(_mapper.Map<IEnumerable<GameCollectionReadDto>>(gameCollections));
        }
        
        //GET api/Collections
        [HttpGet]
        public ActionResult<IEnumerable<CollectionsReadDto>> GetAllCollections()
        {
            IEnumerable<Collections> collectionsItems = _repository.GetAllCollections();

            return Ok(_mapper.Map<IEnumerable<CollectionsReadDto>>(collectionsItems));
        }
        
        //GET api/Collections/{id}
        [HttpGet("{id}")]
        public ActionResult<CollectionsReadDto> GetCollectionById(int id)
        {
            Collections collectionItem = _repository.GetCollectionById(id);
            if (collectionItem != null) return Ok(_mapper.Map<CollectionsReadDto>(collectionItem));
            return NotFound();
        }
        
        //POST api/Collections
        [HttpPost]
        public ActionResult<CollectionsReadDto> CreateCollection(CollectionsCreateDto collection)
        {
            Collections collectionModel = _mapper.Map<Collections>(collection);
            _repository.CreateCollection(collectionModel);
            _repository.SaveChanges();
            
            CollectionsReadDto collectionsReadDto = _mapper.Map<CollectionsReadDto>(collectionModel);
            return CreatedAtRoute(nameof(GetCollectionById), new { Id = collectionsReadDto.Id }, collectionsReadDto);
        }

        //PUT api/Collections/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateCollection(int id, CollectionsUpdateDto collectionsUpdateDto)
        {
            Collections collectionModelFromRepo = _repository.GetCollectionById(id);
            if (collectionModelFromRepo == null)
            {
                return NotFound();
            }

            _mapper.Map(collectionsUpdateDto, collectionModelFromRepo);

            _repository.UpdateCollection(collectionModelFromRepo);

            _repository.SaveChanges();

            return NoContent();
        }
        
        //PATCH api/Collections/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialCollectionUpdate(int id, JsonPatchDocument<CollectionsUpdateDto> patchDoc)
        {
            Collections collectionModelFromRepo = _repository.GetCollectionById(id);
            if (collectionModelFromRepo == null)
            {
                return NotFound();
            }

            CollectionsUpdateDto collectionToPatch = _mapper.Map<CollectionsUpdateDto>(collectionModelFromRepo);
            patchDoc.ApplyTo(collectionToPatch, ModelState);
            if (!TryValidateModel(collectionToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(collectionToPatch, collectionModelFromRepo);

            _repository.UpdateCollection(collectionModelFromRepo);

            _repository.SaveChanges();

            return NoContent();
        }

        //DELETE api/Collections/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteCollection(int id)
        {
            Collections collectionModelFromRepo = _repository.GetCollectionById(id);
            if (collectionModelFromRepo == null)
            {
                return NotFound();
            }
            _repository.DeleteCollection(collectionModelFromRepo);
            _repository.SaveChanges();

            return NoContent();
        }

        [HttpPost("{id}/games")]
        public ActionResult AddGameToCollection(int id, GameCollectionCreateDto gameCollectionAdd)
        {
            GameCollection gameCollection = new GameCollection
            {
                GameId = gameCollectionAdd.GameId,
                CollectionId = id
            };
            _repository.AddGameToCollection(gameCollection);
            _repository.SaveChanges();
            return Ok(_mapper.Map<GameCollectionReadDto>(gameCollection));

        }

        [HttpDelete("{collectionId}/games/{gameId}")]
        public ActionResult RemoveGameFromCollection(int gameId, int collectionId)
        {
            var gameCollectionModelFromRepo = _repository.GetCollectionGame(collectionId, gameId);
            if (gameCollectionModelFromRepo == null)
            {
                return NotFound();
            }
            _repository.RemoveGameFromCollection(gameCollectionModelFromRepo);
            _repository.SaveChanges();
            return NoContent();
        }
    }
}
