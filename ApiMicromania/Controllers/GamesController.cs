﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiMicromania.Data;
using ApiMicromania.Dtos;
using ApiMicromania.Models;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace ApiMicromania.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : Controller
    {
        private readonly IGamesRepo _repository;
        private readonly IMapper _mapper;

        public GamesController(IGamesRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        //Get api/games
        [HttpGet]
        public ActionResult<IEnumerable<GameReadDto>> GetAllGames()
        {
            IEnumerable<Game> gameItems = _repository.GetAllGames();

            return Ok(_mapper.Map<IEnumerable<GameReadDto>>(gameItems));
        }

        //GET api/games/{id}
        [HttpGet("{id}", Name = "GetGameById")]
        public ActionResult<GameReadDto> GetGameById(int id)
        {
            Game gameItem = _repository.GetGameById(id);
            if (gameItem != null)
            {
                return Ok(_mapper.Map<GameReadDto>(gameItem));
            }

            return NotFound();
        }

        //POST api/games
        [HttpPost]
        public ActionResult<GameReadDto> CreateGame(GameCreateDto gameCreateDto)
        {
            Game gameModel = _mapper.Map<Game>(gameCreateDto);
            _repository.CreateGame(gameModel);
            _repository.SaveChanges();

            GameReadDto gameReadDto = _mapper.Map<GameReadDto>(gameModel);

            return CreatedAtRoute(nameof(GetGameById), new {Id = gameReadDto.Id}, gameReadDto);
        }

        //PUT api/games/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateGame(int id, GameUpdateDto gameUpdateDto)
        {
            Game gameModelFromRepo = _repository.GetGameById(id);
            if (gameModelFromRepo == null)
            {
                return NotFound();
            }

            _mapper.Map(gameUpdateDto, gameModelFromRepo);
            
            _repository.UpdateGame(gameModelFromRepo);

            _repository.SaveChanges();

            return NoContent();
        }

        //PATCH api/games/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialGameUpdate(int id, JsonPatchDocument<GameUpdateDto> patchDoc)
        {
            Game gameModelFromRepo = _repository.GetGameById(id);
            if (gameModelFromRepo == null)
            {
                return NotFound();
            }

            GameUpdateDto gameToPatch = _mapper.Map<GameUpdateDto>(gameModelFromRepo);
            patchDoc.ApplyTo(gameToPatch, ModelState);
            if (!TryValidateModel(gameToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(gameToPatch, gameModelFromRepo);

            _repository.UpdateGame(gameModelFromRepo);

            _repository.SaveChanges();

            return NoContent();
        }

        //DELETE api/games/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteGame(int id)
        {
            Game gameModelFromRepo = _repository.GetGameById(id);
            if (gameModelFromRepo == null)
            {
                return NotFound();
            }
            _repository.DeleteGame(gameModelFromRepo);
            _repository.SaveChanges();

            return NoContent();
        }
    }
}
