﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMicromania.Dtos
{
    public class CollectionsCreateDto
    {
        [Required]
        public string Name { get; set; }
    }
}
