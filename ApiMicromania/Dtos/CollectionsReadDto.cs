﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMicromania.Dtos
{
    public class CollectionsReadDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
