﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMicromania.Dtos
{
    public class GameCollectionReadDto
    {
        public GameReadDto Game { get; set; }
    }
}
