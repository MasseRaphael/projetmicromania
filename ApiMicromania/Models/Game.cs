﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMicromania.Models
{
    public class Game
    { 
        [Key]
        public int Id { get; set; }

        [Required]
       [MaxLength(250)]
       public string Name { get; set; }

        [Required]
        public string Genre { get; set; }
        
        public List<GameCollection> Collection { get; set; }
    }
}
