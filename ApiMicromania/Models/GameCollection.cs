﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMicromania.Models
{
    public class GameCollection
    {
        public int GameId { get; set; }

        public Game Game { get; set; }

        public int CollectionId { get; set; }

        public Collections Collection { get; set; }
    }
}