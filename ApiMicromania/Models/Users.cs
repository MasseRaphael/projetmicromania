﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiMicromania.Models
{
    public class Users
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(250)]
        public string Pseudo { get; set; }

        [Required]
        public string Password { get; set; }
        
        public List<Collections> Collections { get; set; }
    }
}
