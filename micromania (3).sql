-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  jeu. 07 jan. 2021 à 20:47
-- Version du serveur :  8.0.18
-- Version de PHP :  7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `micromania`
--

-- --------------------------------------------------------

--
-- Structure de la table `collections`
--

DROP TABLE IF EXISTS `collections`;
CREATE TABLE IF NOT EXISTS `collections` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `UsersId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Collections_UsersId` (`UsersId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `collections`
--

INSERT INTO `collections` (`Id`, `Name`, `UsersId`) VALUES
(2, 'Jeux de Pan Pan', NULL),
(3, 'C\'est moi qui décide', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `gamecollection`
--

DROP TABLE IF EXISTS `gamecollection`;
CREATE TABLE IF NOT EXISTS `gamecollection` (
  `GameId` int(11) NOT NULL,
  `CollectionId` int(11) NOT NULL,
  PRIMARY KEY (`GameId`,`CollectionId`),
  KEY `IX_GameCollection_CollectionId` (`CollectionId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `gamecollection`
--

INSERT INTO `gamecollection` (`GameId`, `CollectionId`) VALUES
(1, 1),
(1, 3),
(2, 3),
(6, 1),
(7, 3),
(8, 1),
(9, 1),
(23, 3);

-- --------------------------------------------------------

--
-- Structure de la table `games`
--

DROP TABLE IF EXISTS `games`;
CREATE TABLE IF NOT EXISTS `games` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Genre` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `games`
--

INSERT INTO `games` (`Id`, `Name`, `Genre`) VALUES
(1, 'Cyberpunk 2077', 'RPG'),
(2, 'Assassin\'s Creed : Valhalla', 'RPG'),
(3, 'Among Us', 'Indépendant'),
(4, 'SnowRunner', 'Simulation'),
(5, 'Spider-Man : Miles Morales', 'Action'),
(6, 'Halo Infinite', 'FPS'),
(7, 'World Of Warcraft Shadowland', 'MMORPG'),
(8, 'Satisfactorie', 'Survie'),
(9, 'Star Wars Squadron', 'Simulation'),
(10, 'Rocket League', 'Sport'),
(23, 'Star Wars : The Old Republic', 'MMORPG');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pseudo` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Password` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `__efmigrationshistory`
--

DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE IF NOT EXISTS `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `__efmigrationshistory`
--

INSERT INTO `__efmigrationshistory` (`MigrationId`, `ProductVersion`) VALUES
('20201220115332_InitialMigration', '3.1.8'),
('20210104095715_UpdateDatabase', '3.1.8'),
('20210104101740_UpdateDatabasev2', '3.1.8'),
('20210104101845_UpdateDatabaseV3', '3.1.8'),
('20210104103643_UpdateDatabaseV4', '3.1.8'),
('20210104134454_BackOldDatabase', '3.1.8'),
('20210104141817_UpdateDatabaseV5', '3.1.8'),
('20210107140544_UpdateDatabaseV6', '3.1.8');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
